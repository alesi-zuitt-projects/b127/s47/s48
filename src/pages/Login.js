import {Fragment, useEffect, useState, useContext} from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(){
		//useContext is a react hook used to unwrap our context. It will return the data passed as values by a provider component in App.js
		const { user, setUser} = useContext(UserContext)
	
		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');
		const [isActive, setIsActive] = useState(false);
		console.log(email)
		console.log(password)

		useEffect(() => {
			if(email !== '' && password !==''){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [email, password])

		function loginUser(e){
			e.preventDefault();
			//to clear the data in the input fields
			Swal.fire(
			  {
			  	title: 'Yayy!',
			  	icon: 'success',
			  	text: 'Successfully Logged In!!!'
			  }
				)
			//allows us to save data within our browser as string
			// the setItem() method of the Storage interface, when passed a key name and value, will add that key to the given storage object, or update the key's value if its alreadt exists
			//setItem is used to store data in the localStorage as string
			//setItem('key', value)

			localStorage.setItem('email', email);
			setUser({email: email})
			setEmail('')
			setPassword('')
			
		}

		

		//Two way binding
		// the values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is called two way binding
		//the data we changed in the view has updated the state
		//the data in the state has updated the view
	return(

	<Fragment>
		<h1> Login </h1>	
		<Form onSubmit = {(e) => loginUser(e)}> 
			<Form.Group>
				<Form.Label> Email Address: </Form.Label>
				<Form.Control 
				type = "email" 
				placeholder = "Enter Email"
				value= {email}
				onChange={e => setEmail(e.target.value)}
				required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label> Password: </Form.Label>
				<Form.Control 
				type = "password" 
				placeholder = "Enter Password"
				value= {password}
				onChange={e => setPassword(e.target.value)}
				required
				/> 
			</Form.Group>
			{isActive ?
			<Button variant="primary" type="submit" id="submitBtn">Submit </Button>
			:
			<Button variant="primary" disabled type="submit" id="submitBtn">Submit </Button>
			}
		</Form>
		</Fragment>
		)
}
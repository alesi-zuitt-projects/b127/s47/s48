// Bootstrap
import {Card, Button } from 'react-bootstrap';

import PropTypes from 'prop-types'

import {useState, useEffect} from 'react'

export default function CourseCard({courseProp}) {
	//check to see if the data passed properly
	//console.log(courseProp)
	//console.log(typeof courseProp)
	//Deconstruct the course properties into their own variables(destructuring)
	const { name, description, price} = courseProp

	//use the state hook for this component to be able to store its tate
	//states are used to keep track of information related to individual to individual component
	//Syntax
		//const [getter,setter] = useState(initialGetter Value)
		//getter = stored initial(default value)
		//setter = updated value
	const [count, setCount] = useState(0)
	const [seat, seatCount] = useState(10)
	//statehook that indicates the button for enrollment
	const [isOpen, setIsOpen]= useState(true)
	useEffect(()=>{
		if(seat === 0){
			setIsOpen(false);
		}
	}, [seat])


	console.log(useState(0))
	function enroll(){
		
		console.log('Enrollees: ' + count)
		if (count === 10){
			alert("maximum capacity reached")
		}else{
			setCount(count + 1)
			seatCount(seat - 1)
		} 
	}
	/*function seats(){
		seatCount(seat+1);
		console.log('Seats: ' + seat)
		if (count + 1){
		seatCount( seat-1)
		console.log('Seats: ' - seat)
	}
	}*/



	return(
		
				<Card className="cardCourseCard p-3">
					<Card.Body>
						<Card.Title><h2>{name}</h2></Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle><h5>Price</h5></Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						<Card.Text>Enrollees: {count} </Card.Text>
						<Card.Text>Seats: {seat} </Card.Text>
						{isOpen ?
							<Button variant="primary" onClick = {enroll}>Enroll</Button>
							:
							<Button variant="primary" disabled>Enroll</Button>
						}
						
					
					</Card.Body>
				</Card>	
			
		
	)
}

//check if the coursecard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers to ensure the correct information is passed from one component to next

CourseCard.propTypes = {
	// the 'shape' method is use to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

//{props.courseProp.description}
//{props.courseProp.name}
//{props.courseProp.price}